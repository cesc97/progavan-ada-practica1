#ifndef CARTA_H
#define CARTA_H
#include <string>
using namespace std;


class Carta
{
public:
    Carta(int,int);
    virtual ~Carta();
    string static donaNom(int);
    string static donaPal(int);
    void visualitza(bool);
    int getPal();
    string getNom();
    int getNum();

protected:

private:
    int num;
    int npal;
    string nom;
};

#endif // CARTA_H
