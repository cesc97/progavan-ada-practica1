#ifndef TAULA_H
#define TAULA_H
#include <baralla.h>
#include <Carta.h>

class Taula
{
public:
    Taula();
    virtual ~Taula();
    void visualitza(bool);
    void extreureCarta();
    void aparellar(int);
    bool fiJocExit();
    bool hiHaParelles();
    Carta* getCarta(int);
    int getQuantesTaulell();
    int getQuantesMunt();




protected:

private:
    Carta* taulell[20];
    Baralla* munt;
    int posicions;


};

#endif // TAULA_H
