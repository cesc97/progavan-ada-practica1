#ifndef BARALLA_H
#define BARALLA_H
#include "Carta.h"

class Baralla
{
public:
    Baralla();
    virtual ~Baralla();
    Carta* getBaralla(int);
    int getQuantes();
    Carta * extreureCarta();
    bool barallaBuida();
    void visualitza(bool);
protected:

private:
    Carta* baralla[40];
    int quantitat;

};

#endif // BARALLA_H
