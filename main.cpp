#include <iostream>
#include "Carta.h"
#include "baralla.h"
#include"Taula.h"
using namespace std;

int main()
{
    Taula* t= new Taula();
    t->extreureCarta();
    t->extreureCarta();
    t->extreureCarta();

    t->visualitza(true);
    while(!(t->fiJocExit()))
    {

        cout<<"parelles possibles? "<<(t->hiHaParelles()?"SI":"NO")<<endl;//? equival if else
        cout<<"****************************************************************"<<endl;

        cout<<"que vols fer? extreure carta(1)|aparellar(2)"<<endl;
        int m;
        cin>>m;
        if(m==1)
        {
            t->extreureCarta();
        }
        if(m==2)
        {
            cout<<"quines cartes vols aparellar?"<<endl;
            int p;
            cin>>p;
            t->aparellar(p);
        }

        t->visualitza(true);
        cout<<"cartes a la baralla: "<<t->getQuantesMunt()<<endl;
    }
    delete t;
    return 0;
}
