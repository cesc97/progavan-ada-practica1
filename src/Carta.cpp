#include "Carta.h"
#include <iostream>

using namespace std;

Carta::Carta(int palnum,int numero)
{

    num=numero;
    npal=palnum;
    nom= donaNom(num)+" DE "+donaPal(palnum);
    //ctor
}
string Carta::getNom()
{
    return this->nom;
}
int Carta::getNum()
{
    return this->num;
}
int Carta::getPal()
{
    return this->npal;
}
string Carta::donaPal(int palnum)
{
    switch(palnum)
    {
    case 1:
        return "COPES";
        break;
    case 2:
        return "ESPASES";
        break;
    case 3:
        return "OROS";
        break;
    case 4:
        return "BASTONS";
        break;
    }
    return "";
}
string Carta::donaNom(int num)
{
    switch(num)
    {
    case 1:
        return "AS";
        break;
    case 2:
        return "DOS";
        break;
    case 3:
        return "TRES";
        break;
    case 4:
        return "QUATRE";
        break;
    case 5:
        return "CINC";
        break;
    case 6:
        return "SIS";
        break;
    case 7:
        return "SET";
        break;
    case 10:
        return "SOTA";
        break;
    case 11:
        return "CAVALL";
        break;
    case 12:
        return "REI";
        break;
    }
    return "";
}
void Carta::visualitza(bool mode)
{
    if(mode)
    {
        cout << this->nom << endl;
    }
    else
    {
        cout << this->num<<" DE " <<this->donaPal(this->npal) << endl;
    }
}
Carta::~Carta()
{
    delete this;
}
