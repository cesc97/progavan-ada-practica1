#include "Taula.h"
#include <baralla.h>
#include <Carta.h>
#include <iostream>

Taula::Taula()
{
    munt = new Baralla();
    posicions=0;
    //ctor
}
void Taula::visualitza(bool manera)
{
    for(int i=0; i<posicions; i++)
    {
        cout<<i<<" ";
        taulell[i]->visualitza(manera);
    }
}

void Taula::extreureCarta()
{
    Carta* c = munt->extreureCarta();
    taulell[posicions]=c;
    posicions=posicions+1;
}

void Taula::aparellar(int pos)
{
    if(taulell[pos]->getNum()==taulell[pos+2]->getNum()||(taulell[pos]->getPal()==taulell[pos+2]->getPal()))
    {
        for(int i= pos; i<this->posicions-1; i+=1)
        {
            taulell[i]= taulell[i+1];
        }
        posicions=posicions-1;
        for(int i= pos+2; i<this->posicions-1; i+=1)
        {
            taulell[i]= taulell[i+1];
        }
        posicions=posicions-1;
    }

}

bool Taula::fiJocExit()
{
    if((this->posicions==20)&&(this->hiHaParelles()==false))
    {
        cout<<"HAS PERDUT"<<endl;
        return true;
    }
    if((munt->barallaBuida())&&(this->getQuantesTaulell()==2))
    {
        cout<<"HAS GUANYAT"<<endl;

        return true;
    }
    return false;
}

bool Taula::hiHaParelles()
{
    if(posicions>2)
    {
        for(int pos=0; pos<this->posicions-2; pos++)
        {
            if(taulell[pos]->getNum()==taulell[pos+2]->getNum()||(taulell[pos]->getPal()==taulell[pos+2]->getPal()))
            {
                return true;
            }
        }
    }
    return false;
}

Carta* Taula::getCarta(int i)
{
    return this->taulell[i];
}

int Taula::getQuantesTaulell()
{
    return this->posicions;
}

int Taula::getQuantesMunt()
{
    return munt->getQuantes();
}

Taula::~Taula()
{
    delete munt;
    for(int i=0; i<posicions; i++)
    {
        delete taulell[i];
    }
}
