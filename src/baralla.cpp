#include "baralla.h"
#include "Carta.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
using namespace std;

Baralla::Baralla()
{
    //ctor
    quantitat=40;
    int c=0;
    for (int i = 1; i < 5; i++)
    {
        for (int j = 1; j < 13; j++)
        {

            if ( j == 8 || j == 9)
            {
                continue;
            }
            baralla[c]= new Carta(i,j);
            c++;
        }

    }
}
Carta* Baralla::getBaralla(int i)
{
    return this->baralla[i];
}
int Baralla::getQuantes()
{
    return this->quantitat;
}
Carta* Baralla::extreureCarta()
{
    srand((unsigned)time(0));
    int pos;
    pos = (rand()%this->quantitat)+1;
    Carta* c= this->baralla[pos];
    baralla[pos]=baralla[this->quantitat-1];
    quantitat=quantitat-1;
    return c;

}
bool Baralla::barallaBuida()
{
    if(quantitat>=1)
    {
        return false;
    }
    else
    {
        return true;
    }
}
void Baralla::visualitza(bool manera)
{
    for (int i = 0; i <this->quantitat; i++)
    {
        Carta* c=  this->getBaralla(i);
        c->visualitza(manera);
    }
}
Baralla::~Baralla()
{
    for(int i=0; i<quantitat; i++)
    {
        delete baralla[i];
    }
}
